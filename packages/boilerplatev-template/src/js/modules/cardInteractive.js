import $ from 'jquery'

export default (el, $el, opts) => {
    const cards = $el.find('.c-card')
    const prev = $el.find('.c-card-interactive__button--prev')
    const next = $el.find('.c-card-interactive__button--next')
    const animationMs = 150
    let activeCard = 0
    let cardsLength = cards.length

    prev.on('click', function (e) {
        e.preventDefault()
        switchCards('prev')
    })

    next.on('click', function (e) {
        e.preventDefault()
        switchCards('next')
    })

    cards.each(function (index) {
        const $element = $(this)
        if (index !== 0) {
            $element.css('opacity', '0')
            $element.css('position', 'absolute')
        } else {
            $element.css('opacity', '1')
            $element.css('position', 'relative')
        }
    })

    function switchCards(direction) {
        const $currentCard = $(cards[activeCard])
        let animationDir
        if (direction === 'next') {
            activeCard = activeCard === cardsLength - 1 ? 0 : activeCard + 1
            animationDir = 'left'
        } else {
            activeCard = activeCard === 0 ? cardsLength - 1 : activeCard - 1
            animationDir = 'right'
        }

        $currentCard.animate({ opacity: 0, [animationDir]: '50px' }, animationMs, function () {
            $currentCard.css('position', 'absolute')
            $currentCard.css(animationDir, '')
            $(cards[activeCard])
                .css('position', 'relative')
                .css(animationDir, '-50px')
                .animate({ opacity: 1, [animationDir]: 0 }, animationMs)
        })
    }
}