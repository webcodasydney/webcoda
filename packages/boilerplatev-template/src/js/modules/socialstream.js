import SocialStream from '@/utils/SocialStream';

export default (el, $el, opts) => {
	new SocialStream($el, opts);
}
