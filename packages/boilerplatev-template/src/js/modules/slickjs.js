import 'slick-carousel'

export default (el, $el, opts) => {
	// custom options. set based on opts.key
	const $parent = $el.parent()
	const jsSlickOptions = {}

	const options = {
		rows: 0,
		sliderCaptionSelector: '.o-slider__caption',
		sliderCounterActiveSlideNumberSelector: '.o-slider__counter-index',
		sliderCounterTotalSelector: '.o-slider__counter-total',
		...(opts.key ? jsSlickOptions[opts.key] : {}),
		...opts,
	}

	const genericSlideCounts = $el.children().length

	options.arrows = genericSlideCounts > 1 && options.arrows
	options.dots = genericSlideCounts > 1 && options.dots

	// set initial caption
	$parent.find(options.sliderCaptionSelector).html(
		$el
			.children()
			.eq(0)
			.data('caption'),
	)

	// set count in the slider
	$parent.find(options.sliderCounterTotalSelector).html(genericSlideCounts)

	options.beforeInit && options.beforeInit(options)

	$el.on('init', (ev, slick) => {
		if (options?.onInit) {
			options.onInit(ev, slick, options)
		}
	})
		.on('breakpoint', (ev, slick) => {
			if (options?.onBreakpoint) {
				options.onBreakpoint(ev, slick, options)
			}
		})
		.on('afterChange', (ev, slick, currentSlide) => {
			// set active slide number
			$parent.find(options.sliderCounterActiveSlideNumberSelector).html(currentSlide + 1)

			// set active caption
			$parent.find(options.sliderCaptionSelector).html(slick.$slides[currentSlide]?.dataset?.caption ?? '')

			if (options?.onAfterChange) {
				options.onAfterChange(ev, slick, currentSlide, options)
			}
		})
		.on('setPosition', (ev, slick) => {
			if (options?.onSetPosition) {
				options.onSetPosition(ev, slick, options)
			}
		})
		.on('beforeChange', (ev, slick, currentSlide, nextSlide) => {
			if (options?.onBeforeChange) {
				options.onBeforeChange(ev, slick, currentSlide, nextSlide, options)
			}
		})
		.slick(options || {})
}
