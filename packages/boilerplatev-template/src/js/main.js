'use strict'

/**
 * NOTES:
 * This boilerplate uses CDN/external for jQuery/React package. Configured in gulp/webpack.js.
 * This to prevent the needs of jQuery/React from node_modules
 */

///////////////
// Libraries //
///////////////

///////////////////
// Umbraco Forms //
///////////////////
/**
 * 	There are two types umbraco forms: Umbraco Forms and Contour Forms
 * 	The one we're using is the Umbraco Forms.
 * 	The source of below scripts are from SundownersOverland.Web/App_Plugins/UmbracoForms/Assets/themes/default/*.js
 *
 *  Countour Form ones are from SundownersOverland.Web/App_Plugins/UmbracoForms/Assets/*.js
 */
// import './custom_vendors/umbracoforms-dependencies';
// import './custom_vendors/umbracoforms';
// import './custom_vendors/umbracoforms-conditions';

// Don't know why babel doesn't transpile ssm. Changed to the minified one
import $ from 'jquery'
// import ssm from 'simplestatemanager';
import feature from 'feature.js'

/////////////
// Plugins //
/////////////
import 'bootstrap/js/src/collapse'
import 'bootstrap/js/src/dropdown'
import 'bootstrap/js/src/tab'


////////////
// UTILS //
////////////
import '@/utils/lazysizes' //this will load lazysizes
import { addInitialPluginsAndHydrate } from '@/utils/conditioner'

// Note: Snippet below is to set Webpack public path dynamically so that it loads the utils from the folder where the main.js is located
// If webpack public path is set incorrectly or the dynamic assets don't load, try the snippet below
// const bundleSrc = $('[src*="www_shared/assets/js/main.js"]').attr('src')
// if (bundleSrc) {
// 	__webpack_public_path__ = bundleSrc.substr(0, bundleSrc.lastIndexOf('/') + 1)
// }

if (!feature.touch) {
	import(/* webpackMode: "lazy" */ /* webpackChunkName: "smoothscroll" */ 'smoothscroll-for-websites').then(
		({ default: SmoothScroll }) => {
			SmoothScroll({ animationTime: 800 })
		},
	)
}

///////////////////////////////
// Dynamic Lazy Load Modules //
///////////////////////////////
addInitialPluginsAndHydrate(document.documentElement)

$(() => {

	//////////////////////
	// Responsive Table //
	//////////////////////
	const $table = $('.o-richtext table:not([class])')
	if ($table.length) {
		$table.wrap('<div class="u-table-responsive">')
	}

	/////////
	// SSM //
	/////////
	// ssm.addStates([{
	// 	id: 'xs',
	// 	query: '(max-width: 767px)',
	// 	onEnter: () => {
	// 		console.info('enter <=767px');
	// 	},
	// 	onLeave: () => {
	// 		console.info('leave <=767px');
	// 	},
	// }, {
	// 	id: 'sm',
	// 	query: '(max-width: 991px)',
	// 	onEnter: () => {
	// 		console.info('enter <=991');
	// 	},
	// 	onLeave: () => {
	// 		console.info('leave <=991');
	// 	}
	// }, {
	// 	id: 'md',
	// 	query: '(max-width: 1199px)',
	// 	onEnter: () => {
	// 		console.info('enter <=1199px');
	// 	},
	// 	onLeave: () => {
	// 		console.info('leave <= 1199px');
	// 	}
	// }, {
	// 	id: 'desktop',
	// 	query: '(min-width: 992px)',
	// 	onEnter: () => {
	// 		console.info('enter >= 992px');
	// 	},
	// 	onLeave: () => {
	// 	}
	// } ]);
})
