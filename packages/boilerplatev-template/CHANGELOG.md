# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.13](http://bitbucket.org/webcodasydney/webcoda/compare/@webcoda/boilerplatev-template@0.0.12...@webcoda/boilerplatev-template@0.0.13) (2020-04-07)

**Note:** Version bump only for package @webcoda/boilerplatev-template





## [0.0.12](http://bitbucket.org/webcodasydney/webcoda/compare/@webcoda/boilerplatev-template@0.0.11...@webcoda/boilerplatev-template@0.0.12) (2020-03-31)

**Note:** Version bump only for package @webcoda/boilerplatev-template





## [0.0.11](http://bitbucket.org/webcodasydney/webcoda/compare/@webcoda/boilerplatev-template@0.0.10...@webcoda/boilerplatev-template@0.0.11) (2020-03-31)

**Note:** Version bump only for package @webcoda/boilerplatev-template





## [0.0.10](http://bitbucket.org/webcodasydney/webcoda/compare/@webcoda/boilerplatev-template@0.0.9...@webcoda/boilerplatev-template@0.0.10) (2020-03-31)

**Note:** Version bump only for package @webcoda/boilerplatev-template





## [0.0.9](http://bitbucket.org/webcodasydney/webcoda/compare/@webcoda/boilerplatev-template@0.0.8...@webcoda/boilerplatev-template@0.0.9) (2020-03-30)

**Note:** Version bump only for package @webcoda/boilerplatev-template






## [0.0.8](http://bitbucket.org/webcodasydney/webcoda/compare/@webcoda/boilerplatev-template@0.0.7...@webcoda/boilerplatev-template@0.0.8) (2020-03-23)


### Bug Fixes

* **boilerplatev-template:** add icons for audioplayer in the right place ([87d9e49](http://bitbucket.org/webcodasydney/webcoda/commits/87d9e490e1eac8eedd48ce231713574d61f26a94))
* **boilerplatev-template:** include missing css and moved socialstream to modules folder instead ([4a5b1aa](http://bitbucket.org/webcodasydney/webcoda/commits/4a5b1aa06479c83915e1f818df40d51975a5e842))
* **boilerplatev-template:** sass config ([ab57126](http://bitbucket.org/webcodasydney/webcoda/commits/ab57126505e3577e02dc7d49ce9517249a386552))
* **boilerplatev-template:** update on layout.cshtml and generic.bootstrap.scss ([92643db](http://bitbucket.org/webcodasydney/webcoda/commits/92643db0c2962c728106a720cfc8cbe88f56ec0e))
* **boilerplatev-template:** webpack config to exclude files from babel transpilation correctly ([3df804f](http://bitbucket.org/webcodasydney/webcoda/commits/3df804f4fdec78f2e3e39658b54050d4b8d5dafb))





## [0.0.7](http://bitbucket.org/webcodasysdney/webcoda/compare/@webcoda/boilerplatev-template@0.0.6...@webcoda/boilerplatev-template@0.0.7) (2020-01-23)

**Note:** Version bump only for package @webcoda/boilerplatev-template





## [0.0.6](http://bitbucket.org/webcodasysdney/webcoda/compare/@webcoda/boilerplatev-template@0.0.4...@webcoda/boilerplatev-template@0.0.6) (2020-01-23)

**Note:** Version bump only for package @webcoda/boilerplatev-template





## [0.0.5](http://bitbucket.org/webcodasysdney/webcoda/compare/@webcoda/boilerplatev-template@0.0.4...@webcoda/boilerplatev-template@0.0.5) (2020-01-23)

**Note:** Version bump only for package @webcoda/boilerplatev-template





## [0.0.4](http://bitbucket.org/webcodasysdney/webcoda/compare/@webcoda/boilerplatev-template@0.0.3...@webcoda/boilerplatev-template@0.0.4) (2020-01-15)

**Note:** Version bump only for package @webcoda/boilerplatev-template





## [0.0.3](http://bitbucket.org/webcodasysdney/webcoda/compare/@webcoda/boilerplatev-template@0.0.2...@webcoda/boilerplatev-template@0.0.3) (2020-01-07)

**Note:** Version bump only for package @webcoda/boilerplatev-template





## 0.0.2 (2020-01-07)

**Note:** Version bump only for package @webcoda/boilerplatev-template





## 1.0.4 (2020-01-07)


### Bug Fixes

* packages publishConfig and git url ([ec23a44](http://bitbucket.com/webcodasysdney/webcoda/commits/ec23a44a85eb28809e89861e70f07f3d8146bd5e))



## 1.0.3 (2020-01-07)



## 1.0.2 (2020-01-07)





## [1.0.3](https://bitbucket.org/webcodasydney/webcoda/compare/v1.0.2...v1.0.3) (2020-01-07)

**Note:** Version bump only for package @webcoda/boilerplatev-template





## 1.0.2 (2020-01-07)

**Note:** Version bump only for package @webcoda/boilerplatev-template





## 1.0.1 (2020-01-07)

**Note:** Version bump only for package @webcoda/boilerplatev-template
