const path = require('path')

const docSiteUrl =
	process.env.DEPLOY_PRIME_URL || 'https://vue-styleguidist.github.io'

module.exports = {
	title: 'Webcoda Design System',
	components: [
		'node_modules/@webcoda/boilerplatev-template/src/js/components/**/[A-Z]*.vue',
		'../boilerplatev-template/src/js/components/**/[A-Z]*.vue'
	],
	version: require('./package.json').version,
	ribbon: {
		text: 'Back to examples',
		url: `${docSiteUrl}/Examples.html`
	},
	/**
	 * Enabling the following option splits sections into separate views.
	 */
	pagePerSection: true,
	simpleEditor: true,
	template: {
		title: 'Example — Webcoda Design System',
		lang: 'en',
		trimWhitespace: true,
		head: {
			meta: [
				{
					name: 'viewport',
					content: 'width=device-width,initial-scale=1.0'
				},
				{
					name: 'format-detection',
					content: 'telephone=no'
				}
			]
		}
	},
	renderRootJsx: path.join(__dirname, 'docs/components/Preview.js'),
	/**
	 * static assets directory
	 */
	assetsDir: 'docs/assets',

	/**
	 * remove the component pathline
	 */
	getComponentPathLine: () => '',
	/**
	 * load the vueds color scheme
	 */
	...require('./docs/vueds-theme'),
	/**
	 * import the webpack config from the project. It might be useful to add
	 * a babel loader to gety the jsx to work properly for custom components
	 */
	webpackConfig: require('./webpack.config'),
	/**
	 * We’re defining below JS and SCSS requires for the documentation.
	 */
	require: [
		/**
		 * load the custom font
		 */
		// path.join(__dirname, 'styleguide/loadfont.js'),
		'codemirror/lib/codemirror.css',
		path.join(__dirname, 'docs/docs.styles.scss'),
		path.join(__dirname, 'docs/docs.helper.js'),
		'@webcoda/boilerplatev-template/src/css/main.scss',
		'@webcoda/boilerplatev-template/src/js/main.js'
	],
	/**
	 * Make sure sg opens with props and examples visible
	 */
	usageMode: 'expand',
	exampleMode: 'expand',
	// styleguideComponents: {
	// 	ReactComponentRenderer: path.join(
	// 		__dirname,
	// 		'styleguide/components/ReactComponent'
	// 	),
	// 	PlaygroundRenderer: path.join(
	// 		__dirname,
	// 		'styleguide/components/Playground'
	// 	)
	// },
	styleguideDir: 'dist',
	sections: [
		{
			name: 'Getting Started',
			content: './docs/getting-started.md',
			sectionDepth: 1
		},
		{
			name: 'Utility Classes',
			content: './docs/utility-classes.md',
			sectionDepth: 1
		},
		{
			name: 'Typography',
			content: './docs/typography.md',
			sectionDepth: 1
		},
		{
			name: 'Elements',
			content: './docs/elements.md',
			pagePerSection: true,
			components: [
				'node_modules/@webcoda/boilerplatev-template/src/js/components/**/[A-Z]*.vue',
				'../boilerplatev-template/src/js/components/**/[A-Z]*.vue'
			],
			sectionDepth: 2
		}
	]
}
