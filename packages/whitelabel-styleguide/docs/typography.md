```jsx
<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<h5>Heading 5</h5>
<h6>Heading 6</h6>

<br />

<h1 class="h6">Heading 1 with class h6</h1>
<h2 class="h5">Heading 2 with class h5</h2>
<h3 class="h4">Heading 3 with class h4</h3>
<h4 class="h3">Heading 4 with class h3</h4>
<h5 class="h2">Heading 5 with class h2</h5>
<h6 class="h1">Heading 6 with class h1</h6>

<p class="lead">This is a lead paragraph</p>

```
