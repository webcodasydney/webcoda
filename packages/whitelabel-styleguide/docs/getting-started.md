### Webcoda Design System is an open source tool for building UI Design Systems with Vue.js and HTML. It provides you and your team a set of organized tools, patterns & practices that work as the foundation for your application development.


All the components are located in [Boilerplatev Template](https://bitbucket.org/webcodasydney/webcoda/src/master/packages/boilerplatev-template/) project which will be the based for [Generator Boilerplatev2](https://bitbucket.org/webcodasydney/webcoda/src/master/packages/generator-boilerplatev2/) and this [styleguide](https://bitbucket.org/webcodasydney/webcoda/src/master/packages/whitelabel-styleguide/)
