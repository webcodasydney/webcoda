Note: Every spacing class use baseline as it's multiplier. Default baseline is 4px

It is really recommended to use utility classes as much as possible. It will:

1. Make your development faster
1. Minimise CSS file (really great combined with PurgeCSS)

For the utility classes, [Tailwind CSS](https://tailwindcss.com/) is used because:

1. The configuration is made flexible and it's in Javascript
1. Better community support
1. Great documentation

Configuration file is `tailwind.config.js` and it's located in the root of the bootstraped folder

In the boilerplate, the tailwind.config.js contains full configuration (including default configuration), to know which options can be configured easily

In the boilerplatev-template, there's some custom configurations to accomodate the development that are worthy to be noted:

1. `separator`: default is colon (:), but dash (-) is used instead for Pug compatibility
1. `theme.screens`: matched with Bootstrap 3, but this can be configured per project need
1. `plugins`: added [tailwind-bootstrap-grid](https://github.com/karolis-sh/tailwind-bootstrap-grid), to use Tailwind configuration (_note_: Use tailwind syntax)
1. `theme.fontWeight`: using numbers instead of name (e.g. `font-700` instead of `font-bold`)
1. etc...

To compare the configuration file with the default configuration

1. run `npx tailwind init tailwind.default.js --full`
1. compare `tailwind.default.js` with `tailwind.config.js`
