# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.13](http://bitbucket.org/webcodasydney/webcoda/compare/@webcoda/whitelabel-styleguide@0.0.12...@webcoda/whitelabel-styleguide@0.0.13) (2020-04-07)

**Note:** Version bump only for package @webcoda/whitelabel-styleguide





## [0.0.12](http://bitbucket.org/webcodasydney/webcoda/compare/@webcoda/whitelabel-styleguide@0.0.11...@webcoda/whitelabel-styleguide@0.0.12) (2020-03-31)

**Note:** Version bump only for package @webcoda/whitelabel-styleguide





## [0.0.11](http://bitbucket.org/webcodasydney/webcoda/compare/@webcoda/whitelabel-styleguide@0.0.10...@webcoda/whitelabel-styleguide@0.0.11) (2020-03-31)

**Note:** Version bump only for package @webcoda/whitelabel-styleguide





## [0.0.10](http://bitbucket.org/webcodasydney/webcoda/compare/@webcoda/whitelabel-styleguide@0.0.9...@webcoda/whitelabel-styleguide@0.0.10) (2020-03-31)

**Note:** Version bump only for package @webcoda/whitelabel-styleguide





## [0.0.9](http://bitbucket.org/webcodasydney/webcoda/compare/@webcoda/whitelabel-styleguide@0.0.8...@webcoda/whitelabel-styleguide@0.0.9) (2020-03-30)

**Note:** Version bump only for package @webcoda/whitelabel-styleguide






## [0.0.8](http://bitbucket.org/webcodasydney/webcoda/compare/@webcoda/whitelabel-styleguide@0.0.7...@webcoda/whitelabel-styleguide@0.0.8) (2020-03-23)


### Bug Fixes

* **boilerplatev-template:** add icons for audioplayer in the right place ([87d9e49](http://bitbucket.org/webcodasydney/webcoda/commits/87d9e490e1eac8eedd48ce231713574d61f26a94))
* **whitelabel-styleguide:** remove icons from docs/assets since this folder was for Vue Design System originally ([4e9548b](http://bitbucket.org/webcodasydney/webcoda/commits/4e9548b7588b3e402a2a5b1db11e2c5d5c19e41d))
* **whitelabel-styleguide:** sass config ([fc91a35](http://bitbucket.org/webcodasydney/webcoda/commits/fc91a3533c5231f651f7a5925d85f2ab77c05e1c))
* **whitelabel-styleguide:** webpack config to exclude files from babel transpilation correctly ([d411087](http://bitbucket.org/webcodasydney/webcoda/commits/d411087ce5ba6e3e7ae2c14447af17f408b6a83f))





## [0.0.7](http://bitbucket.org/webcodasysdney/webcoda/compare/@webcoda/whitelabel-styleguide@0.0.6...@webcoda/whitelabel-styleguide@0.0.7) (2020-01-23)

**Note:** Version bump only for package @webcoda/whitelabel-styleguide





## [0.0.6](http://bitbucket.org/webcodasysdney/webcoda/compare/@webcoda/whitelabel-styleguide@0.0.4...@webcoda/whitelabel-styleguide@0.0.6) (2020-01-23)

**Note:** Version bump only for package @webcoda/whitelabel-styleguide





## [0.0.5](http://bitbucket.org/webcodasysdney/webcoda/compare/@webcoda/whitelabel-styleguide@0.0.4...@webcoda/whitelabel-styleguide@0.0.5) (2020-01-23)

**Note:** Version bump only for package @webcoda/whitelabel-styleguide





## [0.0.4](http://bitbucket.org/webcodasysdney/webcoda/compare/@webcoda/whitelabel-styleguide@0.0.3...@webcoda/whitelabel-styleguide@0.0.4) (2020-01-15)

**Note:** Version bump only for package @webcoda/whitelabel-styleguide





## [0.0.3](http://bitbucket.org/webcodasysdney/webcoda/compare/@webcoda/whitelabel-styleguide@0.0.2...@webcoda/whitelabel-styleguide@0.0.3) (2020-01-07)

**Note:** Version bump only for package @webcoda/whitelabel-styleguide





## 0.0.2 (2020-01-07)

**Note:** Version bump only for package @webcoda/whitelabel-styleguide





## 1.0.4 (2020-01-07)


### Bug Fixes

* packages publishConfig and git url ([ec23a44](http://bitbucket.com/webcodasysdney/webcoda/commits/ec23a44a85eb28809e89861e70f07f3d8146bd5e))



## 1.0.3 (2020-01-07)



## 1.0.2 (2020-01-07)





## [1.0.3](https://bitbucket.org/webcodasydney/webcoda/compare/v1.0.2...v1.0.3) (2020-01-07)

**Note:** Version bump only for package @webcoda/whitelabel-styleguide





## 1.0.2 (2020-01-07)

**Note:** Version bump only for package @webcoda/styleguidist-whitelabel





## 1.0.1 (2020-01-07)

**Note:** Version bump only for package @webcoda/styleguidist-whitelabel
