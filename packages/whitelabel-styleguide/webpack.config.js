const path = require('path')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
	.BundleAnalyzerPlugin
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin')
const magicImporter = require('node-sass-magic-importer')

const getPostCssPlugins = () =>
	[
		require('tailwindcss')('../boilerplatev-template/tailwind.config.js'),
		require('autoprefixer')(),
		require('rucksack-css')({ reporter: true }),
		require('postcss-pxtorem')({ replace: false }),
		// require('@fullhuman/postcss-purgecss')({
		// 	content: ['./src/*.pug', './src/pug/**/*.pug', './src/js/*/**.vue']
		// }),
		require('cssnano')({
			rebase: false,
			// discardComments: {
			// 	removeAll: true,
			// },
			discardUnused: false,
			minifyFontValues: true,
			filterOptimiser: true,
			functionOptimiser: true,
			minifyParams: true,
			normalizeUrl: true,
			reduceBackgroundRepeat: true,
			convertValues: true,
			discardEmpty: true,
			minifySelectors: true,
			reduceInitial: true,
			reduceIdents: false,
			mergeRules: false,
			zindex: false
		})
	].filter((item) => !!item)

module.exports = {
	resolve: {
		extensions: ['.js', '.vue'],
		alias: {
			'@': '@webcoda/boilerplatev-template/src/js',
			'@@': '@webcoda/boilerplatev-template/src/'
		}
	},

	module: {
		rules: [
			{
				test: /\.pug$/,
				oneOf: [
					{
						resourceQuery: /^\?vue/,
						use: ['pug-plain-loader']
					},
					// this applies to pug imports inside JavaScript
					{
						use: [
							{
								loader: 'pug-loader',
								options: {
									pretty: true
								}
							}
						]
					}
				]
			},
			{
				test: /\.vue$/,
				loader: 'vue-loader'
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: {
					test: /node_modules(\/|\\)(?!(conditioner-core|bootstrap|simplestatemanager)(\/|\\)).*/,
					exclude: [
						path.resolve(
							__dirname,
							'node_modules/@webcoda/boilerplatev-template'
						)
					]
				},
				options: {
					// note: if some modules don't work on IE10/IE11, try loose mode on preset-env
					// example below:
					// presets: [['@babel/preset-env', { loose: true }], '@babel/preset-react'],
					presets: ['@babel/preset-env'],
					plugins: [
						'@babel/plugin-transform-runtime',
						// // Stage 2
						// [
						// 	'@babel/plugin-proposal-decorators',
						// 	{ legacy: true }
						// ],
						// // Stage 3
						[
							'@babel/plugin-proposal-class-properties',
							{ loose: true }
						],
						'@babel/plugin-proposal-optional-chaining',
						'@babel/plugin-proposal-nullish-coalescing-operator'
					]
				}
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader']
			},
			{
				test: /\.scss$/,
				oneOf: [
					// this matches `<style module>`
					{
						resourceQuery: /module/,
						use: [
							'vue-style-loader',
							{
								loader: 'css-loader',
								options: {
									modules: {
										localIdentName:
											'[path][name]---[local]---[hash:base64:5]'
									},
									localsConvention: 'camelCase'
								}
							},
							{
								loader: 'postcss-loader',
								options: {
									ident: 'postcss-scss-module',
									plugins: () => getPostCssPlugins()
								}
							},
							{
								loader: 'sass-loader',
								options: {
									prependData: `
										@import "../boilerplatev-template/src/css/01_settings/**/*.scss";
										@import "../boilerplatev-template/src/css/02_tools/**/*.scss";
									`,
									sassOptions: {
										importer: magicImporter()
									}
								}
							}
						]
					},
					// this matches plain `<style>` or `<style scoped>`
					{
						use: [
							// {
							// 	loader: MiniCssExtractPlugin.loader,
							// 	options: {}
							// },
							'vue-style-loader',
							'css-loader',
							{
								loader: 'postcss-loader',
								options: {
									ident: 'postcss-scss',
									plugins: () => getPostCssPlugins()
								}
							},
							{
								loader: 'sass-loader',
								options: {
									prependData: `
										@import "../boilerplatev-template/src/css/01_settings/**/*.scss";
										@import "../boilerplatev-template/src/css/02_tools/**/*.scss";
									`,
									sassOptions: {
										importer: magicImporter()
									}
								}
							}
						]
					}
				]
			},
			{
				test: /\.(jpe?g|gif|png|svg|woff|woff2|ttf|eot|wav|mp3)$/,
				loader: 'file-loader',
				options: {
					name: '[path][name].[ext]'
				}
			}
		]
	},

	plugins: [
		new VueLoaderPlugin(),
		new SVGSpritemapPlugin('../boilerplatev-template/src/icons/**/*.svg', {
			sprite: {
				prefix: () => ''
			}
		})
	]
}
