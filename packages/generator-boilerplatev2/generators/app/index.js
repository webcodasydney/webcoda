'use strict'

var Generator = require('yeoman-generator')
var updateNotifier = require('update-notifier')
var boilerplatevTemplatePkg = require('@webcoda/boilerplatev-template/package.json')
var printMessage = require('print-message')
var pkg = require('../../package.json')
require('colors')

var notifyUpdate = _pkg => {
	const notifier = updateNotifier({
		pkg: _pkg,
		updateCheckInterval: 0,
		isGlobal: true,
	}).notify({
		defer: false,
		isGlobal: true,
	})

	return notifier.update
}

module.exports = class extends Generator {
	initializing() {
		var x = notifyUpdate(pkg)
		var y = notifyUpdate(boilerplatevTemplatePkg)

		// If there's any update, quit
		if (x || y) {
			process.exit(1)
		}
	}
	prompting() {
		printMessage(['        BoilerplateV2        ', ('        v' + pkg.version).green + '        '], {
			marginBottom: 1,
			paddingTop: 2,
			paddingBottom: 2,
		})

		return this.prompt([
			{
				type: 'input',
				name: 'name',
				message: 'Project Name',
				default: this.appname, // Default to current folder name
			},
			{
				type: 'confirm',
				name: 'replaceRazorLayout',
				message: 'Replace Layout.cshtml with HtmlWebpackPlugin-enabled version (ASP.NET MVC only)',
				default: true,
			},
			{
				type: 'confirm',
				name: 'skipInstall',
				message: 'Do you want to skip yarn install?',
				default: false,
			},
		]).then(answers => {
			this.projectName = answers.name
			this.replaceRazorLayout = answers.replaceRazorLayout
			this.skipInstall = answers.skipInstall
		})
	}
	writing() {
		const config = {
			...this,
			pkg,
			boilerplatevTemplatePkg,
		}

		const { replaceRazorLayout } = this

		const templatePath = str => this.templatePath('../../../node_modules/@webcoda/boilerplatev-template/' + str)

		// Copy Razor Layout if opted-in
		if (replaceRazorLayout) {
			this.fs.copy(templatePath('cshtml/'), this.destinationPath('cshtml/'))
		}

		// Copy all directory
		this.fs.copyTpl(templatePath('!(cshtml)*/**/!(^_|^.gitignore)*'), this.destinationRoot(), config, null, {
			globOptions: { dot: true },
		})

		// Copy all files in the root
		this.fs.copyTpl(templatePath('!(^.gitignore|^.package|CHANGELOG)*.*'), this.destinationRoot(), config, null, {
			globOptions: { dot: true },
		})

		// copy .gitignorefile as .gitignore
		this.fs.copy(templatePath('.gitignorefile'), this.destinationPath('.gitignore'))

		// create package.json
		this.fs.copyTpl(templatePath('.package.json'), this.destinationPath('package.json'), config)
	}
	install() {
		if (!this.skipInstall) {
			this.yarnInstall()
		}
	}
}
