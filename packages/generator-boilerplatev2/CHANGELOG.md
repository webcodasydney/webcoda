# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.14](http://bitbucket.org/webcodasydney/webcoda/compare/generator-boilerplatev2@0.0.13...generator-boilerplatev2@0.0.14) (2020-04-07)

**Note:** Version bump only for package generator-boilerplatev2





## [0.0.13](http://bitbucket.org/webcodasydney/webcoda/compare/generator-boilerplatev2@0.0.12...generator-boilerplatev2@0.0.13) (2020-03-31)

**Note:** Version bump only for package generator-boilerplatev2





## [0.0.12](http://bitbucket.org/webcodasydney/webcoda/compare/generator-boilerplatev2@0.0.11...generator-boilerplatev2@0.0.12) (2020-03-31)

**Note:** Version bump only for package generator-boilerplatev2





## [0.0.11](http://bitbucket.org/webcodasydney/webcoda/compare/generator-boilerplatev2@0.0.10...generator-boilerplatev2@0.0.11) (2020-03-31)

**Note:** Version bump only for package generator-boilerplatev2





## [0.0.10](http://bitbucket.org/webcodasydney/webcoda/compare/generator-boilerplatev2@0.0.9...generator-boilerplatev2@0.0.10) (2020-03-30)

**Note:** Version bump only for package generator-boilerplatev2





## [0.0.9](http://bitbucket.org/webcodasydney/webcoda/compare/generator-boilerplatev2@0.0.8...generator-boilerplatev2@0.0.9) (2020-03-23)

**Note:** Version bump only for package generator-boilerplatev2





## [0.0.8](http://bitbucket.org/webcodasysdney/webcoda/compare/generator-boilerplatev2@0.0.7...generator-boilerplatev2@0.0.8) (2020-01-23)

**Note:** Version bump only for package generator-boilerplatev2





## [0.0.7](http://bitbucket.org/webcodasysdney/webcoda/compare/generator-boilerplatev2@0.0.5...generator-boilerplatev2@0.0.7) (2020-01-23)

**Note:** Version bump only for package generator-boilerplatev2





## [0.0.6](http://bitbucket.org/webcodasysdney/webcoda/compare/generator-boilerplatev2@0.0.5...generator-boilerplatev2@0.0.6) (2020-01-23)

**Note:** Version bump only for package generator-boilerplatev2





## [0.0.5](http://bitbucket.org/webcodasysdney/webcoda/compare/generator-boilerplatev2@0.0.4...generator-boilerplatev2@0.0.5) (2020-01-15)

**Note:** Version bump only for package generator-boilerplatev2





## [0.0.4](http://bitbucket.org/webcodasysdney/webcoda/compare/generator-boilerplatev2@0.0.3...generator-boilerplatev2@0.0.4) (2020-01-09)

**Note:** Version bump only for package generator-boilerplatev2





## [0.0.3](http://bitbucket.org/webcodasysdney/webcoda/compare/generator-boilerplatev2@0.0.2...generator-boilerplatev2@0.0.3) (2020-01-09)

**Note:** Version bump only for package generator-boilerplatev2





## 0.0.2 (2020-01-09)

**Note:** Version bump only for package generator-boilerplatev2





## [0.0.3](http://bitbucket.org/webcodasysdney/webcoda/compare/@webcoda/generator-boilerplatev-webpack@0.0.2...@webcoda/generator-boilerplatev-webpack@0.0.3) (2020-01-07)

**Note:** Version bump only for package @webcoda/generator-boilerplatev-webpack





## 0.0.2 (2020-01-07)

**Note:** Version bump only for package @webcoda/generator-boilerplatev-webpack





## 1.0.4 (2020-01-07)


### Bug Fixes

* packages publishConfig and git url ([ec23a44](http://bitbucket.com/webcodasysdney/webcoda/commits/ec23a44a85eb28809e89861e70f07f3d8146bd5e))



## 1.0.3 (2020-01-07)



## 1.0.2 (2020-01-07)





## [1.0.3](https://github.com/viperfx07/generator-boilerplatev/compare/v1.0.2...v1.0.3) (2020-01-07)

**Note:** Version bump only for package @webcoda/generator-boilerplatev-webpack





## 1.0.2 (2020-01-07)

**Note:** Version bump only for package @webcoda/generator-boilerplatev-webpack





## 1.0.1 (2020-01-07)

**Note:** Version bump only for package @webcoda/generator-boilerplatev-webpack





#### 0.7.0 (2017-08-26)
- Minify scripts by default
- Added chunk hash on chunk scripts. It's useful to invalidate caching on async scripts
- try changelog generator
